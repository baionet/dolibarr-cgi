#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011, 2013-2014, 2017-2019 Samuel Thibault <samuel.thibault@ens-lyon.org>
# Copyright (c) 2012 Guillaume Subiron <maethor@subiron.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import os, sys
lang = os.getenv("LANG")
if lang != "fr_FR.UTF-8":
    os.putenv("LANG","fr_FR.UTF-8")
    os.execv(sys.argv[0], sys.argv)

import cgi
import cgitb
cgitb.enable()
import locale
import html

print("Content-type: text/html; charset=utf-8\r\n\r\n")
locale.setlocale(locale.LC_ALL,"fr_FR.UTF-8")

html.header("Test d'éligibilité ADSL")
html.title2("Test d'éligibilité ADSL")

# On peut arriver ici avec des champs déjà remplis. C'est notamment le cas
# quand on clique sur un lien de souscription depuis l'espace adhérent (plutôt
# qu'intégrer la souscription à l'espace adhérent).

form = cgi.FieldStorage()

champs=["adsltel","cp","tarif"]
champs_hidden=["adherent","personne","prenom","nom","adr","adrbis","ville","tel","mail","prelevement","iban","bic","typend"]
vals={}
for id in champs + champs_hidden:
	if id in form:
		vals[id] = form[id].value
	else:
		vals[id] = ""

# Pré-remplissage depuis cherchend.cgi
if "teltype" in form:
	s = form["teltype"].value
	i = s.find('-')
	if i != -1:
		vals["adsltel"] = s[:i]
		vals["typend"] = s[i+1:]

print("""
<p><a href="recherchend.cgi">Vous ne connaissez pas votre numéro de ligne ? Cliquez ici.</a></p>
<form class="well form-horizontal" action="/cgi-bin/eligibilite.cgi" method="post">
  <div class="control-group">
    <label class="control-label" for="adsltel">Téléphone</label>
    <div class="controls">
      <input type="text" id="adsltel" name="adsltel" size=21 maxlength=21 value="%s" autofocus/>
      <p class="help-block">Votre numéro n'est demandé que pour les besoins du test. Il ne sera en aucun cas conservé par la suite.</p> 
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="cp">Code Postal</label>
    <div class="controls">
      <input type="text" id="cp" name="cp" size=5 maxlength=5 value="%s"/>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">Tarif</label>
    <div class="controls">
      <label class="radio">
        <input type="radio" id="particulier" name="tarif" value="particulier" checked required /> Normal
      </label>
      <label class="radio">
        <input type="radio" id="preferentiel" name="tarif" value="preferentiel" required /> Préférentiel (étudiant, RMI, chômeur, etc.)
      </label>
    </div>
  </div>"""%(vals["adsltel"],vals["cp"]))
for id in champs_hidden:
	print("""<input type="hidden" id="%s" name="%s" value="%s"/>"""%(id,id,vals[id]))
print("""
  <div class="form-actions">
    <input type="submit" class="btn btn-primary" id="submit" name="submit" value="Valider"/>
  </div>
</form>
""")
html.footer()
