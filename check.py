# -*- coding: utf-8 -*-
# Copyright (c) 2011-2013, 2016-2020 Samuel Thibault <samuel.thibault@ens-lyon.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
import unidecode

# Renvoie true si c est un chiffre
def lettre(c):
	return (c >= 'A' and c <= 'Z') or (c >= 'a' and c <= 'z')

# Renvoie true si `s' est composé uniquement de chiffres
def lettres(s):
	for c in s:
		if not lettre(c):
			return False
	return True

# Renvoie true si c est un chiffre
def chiffre(c):
	return c >= '0' and c <= '9'

# Renvoie true si `s' est composé uniquement de chiffres
def chiffres(s):
	for c in s:
		if not chiffre(c):
			return False
	return True

# interdit en sql
forbidden_sql="\"'`"
# permis: !()*+,-./:;<=>?@[]|

forbidden = forbidden_sql
# Renvoie tout caractère interdit dans `s': 0-31, 127-159, TeX, sql
def alphanum(s):
	for c in s:
		if (c<" "):
			# On exclu aussi \r\n ici, pour le dump
			return c
		if (c<=chr(126)):
			if c in forbidden:
				return c
		#if (c>=chr(128)) and (c<chr(160)):
		#	return c


# interdit en TeX
forbidden_tex="#$%&\\^_{}~"
# permis: !"'()*+,-./:;<=>?@[]`|

# Retourne une chaine de charactères convertie en TeX
def texize(s):
	trans = ""
	for c in s:
		if ord(c) >= 256:
			c = unidecode.unidecode(c)
		trans += c
	ret = ""
	for c in trans:
		if c in forbidden_tex:
			ret += "\\"+c
		elif c == '\\':
			ret += "$\\backslash$"
		else:
			ret += c
	return ret

# Vérifie qu'un mot de passe est bon
def passwd(s):
	passmsg ="Le mot de passe doit faire au moins 8 caractères, ne contenir que des lettres de a à z non accentuées ou chiffres, et contenir à la fois des majuscules, des minuscules et des chiffres."
	if len(s) < 8:
		return passmsg
	minuscule = False
	majuscule = False
	chiffre = False
	for c in s:
		ok = False
		if c >= "a" and c <= "z":
			minuscule = True
			ok = True
		if c >= "A" and c <= "Z":
			majuscule = True
			ok = True
		if c >= "0" and c <= "9":
			chiffre = True
			ok = True
		if not ok:
			return passmsg
	if not minuscule or not majuscule or not chiffre:
		return passmsg
	return None

# Renvoie true si `s' est une date JJ/MM/AAAA
def date(s):
	if s[2] != '/':
		return False
	if s[5] != '/':
		return False
	for i in range(0,2)+range(3,5)+range(6,10):
		if not chiffre(s[i]):
			return False
	# Du coup l'ordre lexicographique est correct
	if s[0:2] > "31":
		return False
	if s[3:5] > "12":
		return False
	if len(s) == 10 and s[6:] < "1850":
		return False
	return True

# Renvoie un message d'erreur si `cp' est un code postal invalide
def cp(cp):
	if len(cp) != 5:
		return "Le code postal doit faire 5 chiffres."
	if not chiffres(cp):
		return "Le code postal doit être composé de chiffres."

# vérifie un numéro de compte, et convertit en chiffres pour la clé RIB
def numcompte_chiffres(numcompte):
	if len(numcompte) != 11:
		return None
	ret = ""
	for c in numcompte:
		if c >= "0" and c <= "9":
			ret += c
		elif c >= "A" and c <= "R":
			ret += chr(ord("0")+((ord(c)-ord("A"))%9+1))
		elif c >= "S" and c <= "Z":
			ret += chr(ord("0")+(ord(c)-ord("S")+2))
		else:
			return None
	return ret

# Calcule la clé RIB
def cle_rib(etablissement,guichet,numcompte2):
	total=int(etablissement+guichet+numcompte2+"00")
	return ("%02u"%(97-(total%97)))

# Renvoie un message d'erreur si les paramètres ne forment pas un identifiant
# de compte valide.
def rib(etablissement,guichet,numcompte,clerib):
	if len(etablissement) != 5:
		return "Le numéro d'établissement doit faire 5 chiffres."
	if not chiffres(etablissement):
		return "Le numéro d'établissement doit être composé de chiffres."

	if len(guichet) != 5:
		return "Le numéro de guichet doit faire 5 chiffres."
	if not chiffres(guichet):
		return "Le numéro de guichet doit être composé de chiffres."

	numcompte2 = numcompte_chiffres(numcompte)
	if numcompte2 == None:
		return "Le numéro de compte doit faire 11 chiffres ou lettres majuscules."
	if len(clerib) != 2:
		return "La clé RIB doit faire 2 chiffres."
	if not chiffres(clerib):
		return "La clé RIB doit être composée de chiffres."
	if clerib != cle_rib(etablissement,guichet,numcompte2):
		return "Le numéro d'établissement, de guichet, de compte ou de clé RIB est incorrect."

# vérifie un numéro de compte, et convertit en chiffres pour la clé RIB
def numiban_chiffres(numcompte):
	ret = ""
	for c in numcompte:
		if c >= "0" and c <= "9":
			ret += c
		elif c >= "A" and c <= "Z":
			ret += ("%02u" % (ord(c)-ord("A")+10))
		else:
			return None
	return ret

def check_iban_len(num, pays, adj, normal):
	if num[0:2] == pays:
		if len(num) != normal:
			return "Le numéro de compte IBAN " + adj + " doit avoir " + str(normal) + " charactères"

# Renvoie un message d'erreur si les paramètres ne forment pas un identifiant
# de compte valide.
def iban(numcompte):
	if len(numcompte) <= 4:
		return "Le numéro de compte IBAN doit avoir plus de 4 charactères"
	s = check_iban_len(numcompte, "FR", "français", 27)
	if s:
		return s
	s = check_iban_len(numcompte, "IT", "italien", 27)
	if s:
		return s
	s = check_iban_len(numcompte, "MC", "de Monaco", 27)
	if s:
		return s
	s = check_iban_len(numcompte, "ES", "espagnol", 24)
	if s:
		return s
	s = check_iban_len(numcompte, "AD", "andorain", 24)
	if s:
		return s
	s = check_iban_len(numcompte, "GB", "anglais", 22)
	if s:
		return s
	s = check_iban_len(numcompte, "DE", "allemand", 22)
	if s:
		return s
	s = check_iban_len(numcompte, "CH", "suisse", 21)
	if s:
		return s
	s = check_iban_len(numcompte, "LU", "luxembourgeois", 20)
	if s:
		return s
	numcompte2 = numcompte[4:] + numcompte[0:4]
	numcompte3 = numiban_chiffres(numcompte2)
	if numcompte3 == None:
		return "Le numéro de compte ne doit contenir que des chiffres ou des lettres majuscules."
	total=int(numcompte3)
	if (total % 97) != 1:
	    return "Le numéro IBAN est incorrect."

def bic(num):
	if len(num) != 11 and len(num) != 8:
		return "Le numéro BIC doit avoir 8 ou 11 charactères"

# Renvoie un message d'erreur si ce n'est pas un numéro de téléphone valide
def tel(s):
	if s[0:6] == "VOISIN":
		s = s[6:]
	if s[0:3] == "42C":
		# précâblé
		if len(s) != 5+15 or not lettres(s[3:5]) or not chiffres(s[5:]):
			return "Le numéro de pré-câblé doit faire 2 chiffres, 3 lettres, 15 chiffres (sans espace ou point)."
		return
	if len(s) != 10:
		return "Le numéro de téléphone doit faire 10 chiffres (sans espace ou point)."

	if not chiffres(s):
		return "Le numéro de téléphone doit être composé de chiffres."
	if s[0]!='0':
		return "Le numéro de téléphone doit commencer par 0."
	if s[1] < '1' or s[1] > '9':
		return "Le numéro de téléphone doit commencer par 0[1-9]."

# Renvoie un message d'erreur si ce n'est pas un nom de domaine valide
# TODO: finir (vérifier la règle des 63 charactères entre . par exemple)
def domain(s):
	for c in s:
		if c >= "a" and c <= "z":
			continue
		if c >= "A" and c <= "Z":
			continue
		if c >= "0" and c <= "9":
			continue
		if c == '-' or c == '.':
			continue
		return "Un nom de domaine ne peut contenir que des chiffres, lettres, `-' ou `.'."

# Renvoie un message d'erreur si ce n'est pas une adresse email valide
def mail(s):
	if not "@" in s:
		return "L'adresse email doit contenir une arobase (@)."
	i = s.index("@")
	local = s[:i]
	for c in local:
		if c >= "a" and c <= "z":
			continue
		if c >= "A" and c <= "Z":
			continue
		if c >= "0" and c <= "9":
			continue
		if c in "!#$%&'*+-/=?^_`{|}~.":
			continue
		return "Caractère `"+c+"' interdit dans une adresse email."
	if s[0] == '.':
		return "`.' interdit en tête de la partie locale de l'adresse email."
	if s[len(c)-1] == '.':
		return "`.' interdit en fin de la partie locale de l'adresse email."
	if ".." in s:
		return "`..' interdit dans une adresse email."

	d = s[i+1:]
	if "@" in d:
		return "L'adresse email doit contenir une seule arobase (@)."
	str = domain(d)
	if str:
		return "Adresse e-mail: " + str

def pgp(s):
	s = s.replace(" ","")
	if len(s) > 40:
		return "Trop de caractères dans l'identifiant de clé PGP."
	if len(s) < 40:
		return "Pas assez de caractères dans l'identifiant de clé PGP, il faut utiliser le fingerprint complet."
	for c in s:
		if c >= "a" and c <= "f":
			continue
		if c >= "A" and c <= "F":
			continue
		if c >= "0" and c <= "9":
			continue
		return "Caractère `"+c+"' interdit dans un identifiant de clé PGP."
	ret = os.system("gpg --homedir /srv/www/aquilenet.fr/.gnupg --keyserver hkp://pgp.mit.edu --recv-key "+s+" > /dev/null 2>&1")
	if (ret):
		return "Clé `"+s+"' non trouvée sur le serveur pgp.mit.edu."
