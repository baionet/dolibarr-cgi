#!/usr/bin/python3
# -*- coding: utf-8 -*-
# 
# Copyright (c) 2011, 2013-2020 Samuel Thibault <samuel.thibault@ens-lyon.org>
# Copyright (c) 2012 Guillaume Subiron <maethor@subiron.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 

import os, sys
lang = os.getenv("LANG")
if lang != "fr_FR.UTF-8":
    os.putenv("LANG","fr_FR.UTF-8")
    os.execv(sys.argv[0], sys.argv)

import cgi
import cgitb
cgitb.enable()
import tempfile
import os
import shutil
import locale
import time
import random
import crypt
import mymail

import html
import check
import datetime

# doit contenir un répertoire incoming avec accès en écriture
root="/srv/si"
formulaires=root+"/formulaires"

print("Content-type: text/html; charset=utf-8\r\n\r\n")
locale.setlocale(locale.LC_ALL,"fr_FR.UTF-8")
form = cgi.FieldStorage()
monform = dict((k,form[k].value) for k in form.keys())
if "iban" in monform:
	monform["iban"] = monform["iban"].replace(" ","").replace("\n","").replace("\t","")
if "pgp" in monform:
	monform["pgp"] = monform["pgp"].replace(" ","").replace("\n","").replace("\t","")
if "tel" in monform:
	if monform["tel"].startswith("42C"):
		monform["tel"] = ""
	if monform["tel"].startswith("VOISIN"):
		monform["tel"] = ""

if "tel" not in monform or monform["tel"] == "":
	if "adsltel" in monform and "option" in monform:
		option = monform["option"]
		if option[-11:] == "LiazoOrange" or option[-4:-1] == "Opt":
			monform["tel"] = monform["adsltel"]

# Est-ce que la partie mandat a été remplie ?
mandat = "iban" in monform
# Est-ce que la partie ADSL est remplie ?
adsl = "adsltel" in monform and "tarif" in monform and "option" in monform
# Est-ce qu'un VPN est demandé ?
vpn = "vpn" in monform
# Est-ce que c'est une demande d'ajout de mandat ?
do_prelevement = "prelevement" in monform and monform["prelevement"] == "-1"
# Est-ce qu'il y a besoin d'un mandat ?
prelevement = adsl or do_prelevement
# Est-ce qu'il y a besoin d'une adhésion ?
adhesion = "adherent" not in monform
# Est-ce que c'est un tarif préférentiel qui est demandé
preferentiel = "tarif" in monform and monform["tarif"] == "preferentiel"

# Pas d'erreur pour l'instant
erreur = ""

#cgitb.enable(display=0, logdir="/tmp")

# format: nom champ, libellé du champ, obligatoire ou non

# champs non affichés mais vérifiés
champs_nodisp=[
	("personne","Personne",True),
	("tarif", "Tarif", False),
]
# champs toujours affichés
champs=[
	("prenom","Prénom", False),
	("nom","Nom d'usage ou raison sociale", True),
	("adr", "Adresse", True),
	("adrbis", "Adresse (suite)", False),
	("cp", "Code postal", True),
	("ville", "Ville", True),
	("tel", "Téléphone pour dépannage et compta", "adsltel" in monform and monform["adsltel"].startswith("VOISIN")),
	("mail", "Mail", False),
	("pgp", "Fingerprint PGP éventuel", False),
	#("naissance", "Date de naissance (jj/mm/aaaa)", False)
]
# champs toujours affiché, mais avec des étoiles
champs_password=[
	("passwd", "Mot de passe choisi", True),
	("passwd2", "Mot de passe (encore)", True),
]
# champs affichés seulement quand on a besoin d'un prélèvement
champs_prelevement=[
	("iban", "IBAN", False),
	("bic", "BIC", False)
]
# champs affichés seulement pour une commande ADSL
champs_adsl=[
	("adsltel","Ligne téléphonique ADSL",False),
	("typend","Type de NDI",False),
	("option","Option",False)
]
# champs affichés seulement pour une commande VPN
champs_vpn=[
	("vpn","VPN",False),
]
# champs cachés
champs_caches=["adherent","prelevement"]
# Ce qui va être effectivement écrit dans le fichier à transférer dans Dolibarr
champs_detail=["personne","prenom", "nom", "adr", "adrbis", "cp", "ville", "tel", "mail", "passwd",
#"njour", "nmois", "nannee",
"iban","bic",
"dat","lieu",
"adsltel","tarif","option",
"rum", "vpn", "pgp", "typend"
]

# Imprime un champ dans un formulaire: identifiant en première colonne, champ à
# remplir en deuxième colonne. Si obligatoire est vrai, (*) est ajouté à
# l'identifiant
def print_champ(id, label, obligatoire):
	value=""
	if id[0:6] == 'passwd':
		type="password"
	else:
		type="text"
	if id in monform:
		value=monform[id]
	print("""
  <div class="control-group">
      <label class="control-label" for="%s">%s</label>
      <div class="controls">
         <input type="%s" id="%s" name="%s" value="%s"/>
    """ % (id, label, type, id, id, value))
	if obligatoire:
		print("""<p class="help-inline"><font color="red">*</font></p>""")
	print("""
      </div>
  </div>""")

# Imprime un champ dans un formulaire: identifiant en première colonne, champ déjà rempli
# deuxième colonne.
def print_champ_ro(id, label):
	if not id in monform:
		return
	value = monform[id]
	print("""
  <div class="control-group">
    <label class="control-label" for="%s">%s</label>
    <div class="controls">
      %s
      <input type="hidden" id="%s" name="%s" value="%s"/>
    </div>
  </div>""" % (label, label, value, id, id, value))

# Imprime le formulaire d'adhésion
def formulaire():
	if "adherent" in monform:
		if do_prelevement:
			html.header("Demande d'ajout de mandat")
			html.title2("Demande d'ajout de mandat")
		elif adsl:
			html.header("Demande de souscription à une ligne ADSL")
			html.title2("Demande de souscription à une ligne ADSL")
		elif vpn:
			html.header("Demande de souscription à une connexion VPN")
			html.title2("Demande de souscription à une connexion VPN")
	else:
		html.header("Demande d'adhésion à l'association Aquilenet")
		html.title2("Demande d'adhésion à l'association")
		if adsl:
			print("""<p>Ce formulaire sert également pour vous abonner à une ligne ADSL. Si vous êtes déjà adhérent⋅e, utilisez plutôt le bouton de commande ADSL depuis <a href="https://adherents.aquilenet.fr/adsl.php">votre espace adhérent⋅e</a>.</p>""")
		if vpn:
			print("""<p>Ce formulaire sert également pour vous abonner à une connexion VPN. Si vous êtes déjà adhérent⋅e, utilisez plutôt le bouton de commande VPN depuis <a href="https://adherents.aquilenet.fr/vpn.php">votre espace adhérent⋅e</a>.</p>""")
	if not vpn and not adsl and not do_prelevement:
		if "adsltel" in monform:
			print("""<div class="alert fade in">
              <a class="close" data-dismiss="alert" href="#">×</a>
              <h3>Attention !</h3>
              Vous n'avez pas choisi d'option ou de tarif pour votre ligne ADSL ! Revenez en arrière pour le choisir.
              </div>""");
		print("""<div class="alert alert-info fade in">
              <a class="close" data-dismiss="alert" href="#">×</a>
              Attention, ce formulaire ne sert pas à s'abonner à une ligne ADSL ou une connexion VPN.<br/>
              Il ne sert pas non plus à tester l'éligibilité de votre ligne à l'ADSL.
              </div>""");

		print("""<div class="alert alert-info fade in">
              <a class="close" data-dismiss="alert" href="#">×</a>
              Le <a href=test.cgi>formulaire d'abonnement ADSL</a> couvre également l'adhésion à l'association.<br />
              Le <a href="adhesion.cgi?vpn=1">formulaire d'abonnement VPN</a> couvre également l'adhésion à l'association.<br />
              Ce formulaire-ci ne sert que si vous souhaitez adhérer à l'association mais que vous ne souhaitez pas vous abonner à l'ADSL ou à un VPN. Vous pourrez bien entendu vous abonner à l'ADSL ou à un VPN par la suite si vous le désirez.
              </div>""");


	print("""<fieldset class="well">""")
	if not ("adherent" in monform):
		print("""<legend>Demande d'adhésion</legend>""")
	print("""<form class="form-horizontal" action="/cgi-bin/adhesion.cgi" method="post">""")
	print("""
<div class="help-block">Saisissez votre identité complète (nom et prénom, ou raison sociale, selon le
cas).""")
	if "adherent" in monform:
		print("""Le nom et l'adresse indiqués ici doivent être ceux du (de la) titulaire de la ligne.""")
	else:
		print("""Le nom indiqué ici doit être celui de l'adhérent⋅e à l'association.""")
	print(""" Le mot de passe est à votre choix, il vous servira pour vous connecter aux différents services d'Aquilenet. Seuls les champs marqués d'une étoile rouge sont obligatoires.</div>""")

	# S'il y a eu une erreur pendant un remplissage précédent, l'afficher ici
	if erreur != "":
		print("<div class='alert alert-error'><h3>Attention</h3>%s</div>" % erreur)

	# Formulaire à remplir

	# Champs cachés
	for id in champs_caches:
		if id in monform:
			print("""<input type="hidden" id="%s" name="%s" value="%s"/>"""%(id, id, monform[id]))

	print("""
  <div class="control-group">
    <label class="control-label">Personne</label>
    <div class="controls">
    """)
	for i in ["physique","morale"]:
		checked=""
		if "personne" in monform:
			if monform["personne"] == i:
				checked=" checked"
		print("""
      <label class="radio inline">
        <input type="radio" id="%s" name="personne" value="%s"%s required /> %s
      </label>
        """%(i,i,checked,i))
	print("""
    </div>
  </div>""")

	# Partie adhésion
	for (id,label,obligatoire) in champs:
		print_champ(id,label,obligatoire)

	if not "adherent" in monform:
		print("""<div class="help-block">Au moins 8 caractères, ne contenant que des lettres de a à z non accentuées ou chiffres, et contenant à la fois des majucules, des minuscules et des chiffres</div>""")
		for (id,label,obligatoire) in champs_password:
			print_champ(id,label,obligatoire)

	# Partie prélèvement, seulement si l'adhérent ne l'a pas déjà
	if "prelevement" not in monform or monform["prelevement"] == "-1":
		print("""<h3>Informations bancaires pour les prélèvements</h3>""")
		if adsl:
			print("""
	<div class="help-block">Elles sont obligatoires pour éviter tout souci d'oubli de règlement de la ligne ADSL.</div>""")
		elif vpn:
			print("""
	<div class="help-block">Elles sont recommandées pour éviter tout souci d'oubli de règlement de la connexion VPN.</div>""")
		elif not prelevement:
			print("""
	<div class="help-block">Elles ne sont pas obligatoires, mais vous pouvez les rentrer dès maintenant pour le règlement de futures lignes ADSL ou connexions VPN ou de vos cotisations.</div>""")

		for (id,label,obligatoire) in champs_prelevement:
			print_champ(id,label,obligatoire or ((do_prelevement or adsl) and id != "banqueadrbis"))

	# Partie informations pour l'ADSL, seulement si ça a été choisi avant (adhésion+ADSL, donc)
	if adsl:
		for (id,label,obligatoire) in champs_adsl:
			print_champ_ro(id,label)

	if adsl or vpn:
		if preferentiel:
			part_checked=""
			pref_checked="checked"
		else:
			part_checked="checked"
			pref_checked=""
		print("""
	  <div class="control-group">
	<label class="control-label">Tarif</label>
	<div class="controls">
	  <label class="radio">
		<input type="radio" id="particulier" name="tarif" value="particulier" %s required /> Normal
	  </label>
	  <label class="radio">
		<input type="radio" id="preferentiel" name="tarif" value="preferentiel" %s required /> Préférentiel (étudiant, RMI, chômeur, etc.)
	  </label>
	</div>
	  </div>
""" % (part_checked, pref_checked))
	if vpn:
		print_champ_ro("vpn","VPN")

	# Fin de la table
	print("""
    <div class="form-actions">
        <input type="submit" class="btn btn-primary" id="submit" name="submit" value="Valider"/>
    </div>
</form></fieldset>""")

	html.footer()


# Est-ce que le formulaire a été au moins un peu rempli ?
# (Sinon, c'est que l'utilisateur vient d'arriver sur la page)
def has_anything():
	for id, label, obligatoire in (champs + champs_password + champs_prelevement):
		if id in monform:
			return True
	return False


# Vérifier les champs
def mycheck():
	if not has_anything() or "firstsubmit" in monform:
		# Rien ou arrivée depuis l'éligibilité, rien à vérifier
		formulaire()
		exit()

	# On vérifie tous les champs: alphanumérique seulement, pas démesurément long,
	# et champs obligatoires remplis
	champs_obligatoires = champs_nodisp + champs + champs_prelevement + champs_adsl + champs_vpn
	if "adherent" not in monform:
		champs_obligatoires += champs_password
	for id, label, obligatoire in champs_obligatoires:
		if id in monform:
			err = check.alphanum(monform[id])
			if err:
				return ("« %s » n'est pas autorisé (dans le champ %s)" % (err, label))
			if len(monform[id]) > 128:
				return "Valeur manifestement trop longue."
		else:
			if obligatoire:
				return "Champ %s manquant." % label

	if monform["personne"] == "physique" and not "prenom" in monform:
		return "Le prénom est obligatoire pour les personnes physiques."

	if monform["personne"] == "morale" and "prenom" in monform:
		return "Le prénom ne doit pas être renseigné pour les personnes morales."

	# Codes postaux
	if "cp" in monform:
		string = check.cp(monform["cp"])
		if string:
			return string
	if "banquecp" in monform:
		string = check.cp(monform["banquecp"])
		if string:
			return string

	# Infos IBAN/BIC
	iban_tout = True
	iban_unpeu = False
	for i in [ "iban", "bic" ]:
		iban_tout = iban_tout and i in monform
		iban_unpeu = iban_unpeu or i in monform
	if iban_unpeu:
		if not iban_tout:
			return "Champ manquant dans les informations bancaires."
		string = check.iban(monform["iban"])
		if string:
			return string
		string = check.bic(monform["bic"])
		if string:
			return string

	# Téléphone
	if "tel" in monform:
		string = check.tel(monform["tel"])
		if string:
			return string

	# Téléphone
	if "typend" in monform:
		typend = monform["typend"]
		if typend != "actif" and typend != "inactif" and typend != "precab":
			return "Le type de NDI est incorrect"

	# Date de naissance
	if "naissance" in monform:
		naissance = monform["naissance"]
		if not check.date(naissance):
			return "La date de naissance doit respecter le format jj/mm/aaaa."

	# Addresse e-mail
	if "mail" in monform:
		string = check.mail(monform["mail"])
		if string:
			return string

	# Clé PGP
	if "pgp" in monform:
		string = check.pgp(monform["pgp"])
		if string:
			return string

	if adsl and not (mandat or "prelevement" in monform):
		return "Pour un abonnement ADSL, il faut remplir les informations bancaires."

	if "passwd" in monform:
		if monform["passwd"] != monform["passwd2"]:
			return "Les mots de passe doivent être les mêmes."

		string = check.passwd(monform["passwd"])
		if string:
			return string


# Code principal

# Vérification des erreurs de saisie
erreur = mycheck()
if erreur:
	formulaire()
	exit()

# Formulaire correctement rempli, chouette !
html.header("Commande l'association Aquilenet, suite")
html.title2("Commande, résultat")

# Pour se simplifier la vie, tout ce qui n'est pas rempli est ""
vals={}
for id, label, obligatoire in (champs_nodisp + champs + champs_password + champs_prelevement + champs_adsl + champs_vpn):
	if id in monform:
		vals[id] = monform[id]
	else:
		vals[id] = ""

# Découper la date de naissance
#vals["njour"] = vals["naissance"][0:2]
#vals["nmois"] = vals["naissance"][3:5]
#vals["nannee"] = vals["naissance"][6:10]

# Remplir la date automatiquement. Lieu de signature vide.
vals["dat"] = time.strftime("%x")
vals["lieu"] = ""
vals["rum"] = "AQUILENET " + datetime.datetime.isoformat(datetime.datetime.now()).replace(":","").replace("-","").replace("."," ")

# Crypter le mot de passe avant toute écriture sur disque
salt = ""
for i in range(8):
	n = random.randint(0,63)
	if 0 <= n <= 9:
		salt += chr(n+ord('0'))
	elif 10 <= n <= 35:
		salt += chr(n-10+ord('a'))
	elif 36 <= n <= 61:
		salt += chr(n-36+ord('A'))
	elif n == 62:
		salt += '.'
	elif n == 63:
		salt += '/'

vals["passwd"] = "{crypt}" + crypt.crypt(vals["passwd"], "$1$"+salt)

# Écriture des valeurs au frais pour injecter dans un SI quelconque
(fd,name) = tempfile.mkstemp(".txt","",root+"/incoming")
os.chmod(name, 0o660)
f = os.fdopen(fd, "w")
for champ in champs_detail:
	f.write(vals[champ]+"\n")
f.write("\n")
f.close()



# Génération des pdfs d'adhésion et de mandat
d = tempfile.mkdtemp("","",root+"/tmp")
os.chdir(d)
# pour le cache de fontes
os.putenv("HOME","/home/www-data")

# On patche l'IBAN pour avoir quelque chose de plus lisible
if "iban" in vals:
	newiban = vals["iban"][0:4]
	for i in range(4,len(vals["iban"]),4):
		newiban += " " + vals["iban"][i:i+4]
	vals["iban"] = newiban

def output_tex(basename):
	f = open(basename+".tex","w")
	f.write("\\input "+formulaires+"/"+basename+"-head.inc\n")
	f.write("\\input "+formulaires+"/head.inc\n")
	for champ in champs_detail:
		f.write("\\newcommand{\\%s}{%s}\n" % (champ, check.texize(vals[champ])))
	f.write("\\newcommand{\\nomcomplet}{%s %s}\n" % (vals["prenom"], check.texize(vals["nom"])))
	f.write("\\input "+formulaires+"/"+basename+"-tail.inc\n")
	f.close()

if adhesion:
	output_tex("adhesion")
if mandat:
	output_tex("mandat")
if adsl:
	output_tex("adsl")
if vpn:
	output_tex("vpn")

os.symlink(formulaires+"/logo.pdf", "logo.pdf")

# Lancement pdflatex proprement dit
ret = 0
if adhesion:
	ret = os.system("pdflatex adhesion.tex > adhesion.stdout 2>&1 < /dev/null")
if mandat:
	ret += os.system("pdflatex mandat.tex > mandat.stdout 2>&1 < /dev/null")
if adsl:
	ret += os.system("pdflatex adsl.tex > adsl.stdout 2>&1 < /dev/null")
if vpn:
	ret += os.system("pdflatex vpn.tex > vpn.stdout 2>&1 < /dev/null")

# Ok, affichage du résultat
if ret != 0:
	print('<p>Oops, problème de compilation du formulaire :(, le problème est remonté au (à la) technicien(ne), on s\'en occupe et on vous recontacte.</p>')
	mymail.mymail(To="Samuel Thibault <samuel.thibault@ens-lyon.org>", From="webmaster@aquilenet.fr", Subject="Probleme compilation formulaire", Body="Oops, probleme de compilation de formulaire")
else:
	if (adhesion and (mandat or adsl or vpn)) or (mandat and adsl) or (mandat and vpn):
		pluriel="s"
	else:
		pluriel=""
	print("<p>Document%s généré%s, veuillez le%s télécharger en utilisant le%s lien%s ci-dessous, imprimer, signer et retourner par courrier postal ou scan (ou directement en main propre si vous en avez l'occasion)" % (pluriel, pluriel, pluriel, pluriel, pluriel))

	d2 = tempfile.mkdtemp("","adhesion-","/srv/www/aquilenet.fr/tools/incoming")
	os.chmod(d2, 0o770)
	if adhesion:
		os.rename("adhesion.pdf", d2+"/adhesion.pdf")
	if mandat:
		os.rename("mandat.pdf", d2+"/mandat.pdf")
	if adsl:
		os.rename("adsl.pdf", d2+"/adsl.pdf")
	if vpn:
		os.rename("vpn.pdf", d2+"/vpn.pdf")
	if adhesion:
		print("<p><ul><li><a target=_blank href=\""+d2[27:]+"/adhesion.pdf\">le formulaire d'adhésion prérempli (PDF)</a>,</li>")
	if mandat:
		print("<li><a target=_blank href=\""+d2[27:]+"/mandat.pdf\">le mandat prérempli (PDF)</a>,</li>")
	if adsl:
		print("<li><a target=_blank href=\""+d2[27:]+"/adsl.pdf\">le contrat de souscription ADSL prérempli (PDF)</a>,</li>")
	if vpn:
		print("<li><a target=_blank href=\""+d2[27:]+"/vpn.pdf\">le contrat de souscription VPN prérempli (PDF)</a>,</li>")
	if adhesion and not mandat:
		print("<li>la cotisation (chèque ou virement)</li>")
	if adsl:
		print("</ul><p>ainsi que</p><ul>")
		print("<li>pour le cas non dégroupé ou dégroupé partiellement: une facture France Télécom ou autre document récent certifiant la correspondance téléphone-nom-adresse, par exemple la fiche d'intervention</li>")
		print("<li>pour le cas dégroupage total: un simple justificatif de domicile.</li>")
		if preferentiel:
			print("<li>un justificatif pour le tarif préférentiel</li>")
	if vpn:
		if preferentiel:
			print("<li>un justificatif pour le tarif préférentiel</li>")

	print("</ul>")
	print("""à l'adresse
<pre><tt>Aquilenet
20 rue Tourat
33000 BORDEAUX</tt></pre>

ou 
<pre><tt>tresorier@aquilenet.fr</tt></pre>
(ou bien à l'occasion d'une réunion au local, ou autre), merci !</p>

<p>Si vous avez des interrogations, assurez-vous de jeter un œil à la <a href=http://www.aquilenet.fr/node/8>page du service ADSL</a></p>
""")

shutil.rmtree(d)

html.footer()
