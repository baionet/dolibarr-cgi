#!/usr/bin/python3
# coding: utf8

import os, sys
lang = os.getenv("LANG")
if lang != "fr_FR.UTF-8":
    os.putenv("LANG","fr_FR.UTF-8")
    os.execv(sys.argv[0], sys.argv)

import cgi
import cgitb
from ondes_objets import *
cgitb.enable()

print("Content-Type: text/html; charset=utf-8\r")
print("\r")
print("""
<html>
<head>
<title>Comparaison des ondes</title>
</head>
<body>
""")

form = cgi.FieldStorage()
monform = dict((k,form[k].value) for k in form.keys())

freq_min=1
freq_max=1000000000000000

# Prendre en compte les valeurs passées en http
for objet in objets:
    nom,var,combien,unite,duree,distance,puissance,frequence = objet
    if var+"_combien" in monform:
        objet[2] = float(monform[var+"_combien"])
    if var+"_unite" in monform:
        objet[3] = monform[var+"_unite"] 
    if var+"_duree" in monform:
        objet[4] = monform[var+"_duree"] 
    if var+"_distance" in monform:
        objet[5] = float(monform[var+"_distance"])
    if var+"_puissance" in monform:
        objet[6] = float(monform[var+"_puissance"])

if "freq_min" in monform:
    freq_min = float(monform["freq_min"])
if "freq_max" in monform:
    freq_max = float(monform["freq_max"])

# Afficher l'image
print("""<img src="ondes-img.cgi?""")
for objet in objets:
    nom,var,combien,unite,duree,distance,puissance,frequence = objet
    print("&"+var+"_combien="+str(combien).replace("+","%2B"))
    print("&"+var+"_unite="+unite)
    print("&"+var+"_duree="+duree)
    print("&"+var+"_distance="+str(distance).replace("+","%2B"))
    print("&"+var+"_puissance="+str(puissance).replace("+","%2B"))
print(("&freq_min=%f"%freq_min).replace("+","%2B"))
print(("&freq_max=%f"%freq_max).replace("+","%2B"))
print("""" />""")

print("<p>L'exposition dépend de la distance à l'objet, et de la durée d'exposition. Les valeurs ci-dessous sont des valeurs classiques, à personaliser.</p>")

# Afficher le formulaire
print("""<form action="ondes.cgi" method="post">""")

def print_unites(choisi):
    for i in ("seconde", "minute", "heure", "jour", "semaine", "mois", "annee"):
        if choisi == i:
            select = " selected"
        else:
            select = ""
        print("""<option value="%s" %s>%s</option>"""%(i, select, i))

print("<table border=0>")
for nom,var,combien,unite,duree,distance,puissance,frequence in objets:
    if frequence < freq_min:
        continue
    if frequence > freq_max:
        continue
    print("<tr>")
    print ("<td>")
    nom = nom.replace("R.", "Relais ");
    nom = nom.replace("C.", "Client ");
    nom = nom.replace("A.", "Appel ");
    print(nom+": ")
    print ("</td>")

    print ("<td>")
    print("""<input name="%s_combien" value="%g" size=2 />""" % (var, combien))
    print("""<select name="%s_unite" value="%s">""" % (var, unite))
    print_unites(unite)
    print("""</select>""")
    print(" par ")
    print("""<select name="%s_duree" value="%s">""" % (var, duree))
    print_unites(duree)
    print("""</select>""")
    print(" à ")
    print("""<input name="%s_distance" value="%g" size=4 />""" % (var, distance))
    print("m, ")
    print("""PIRE <input name="%s_puissance" value="%g" size=2 /> W,""" % (var, puissance))
    if (frequence > 800000000):
        print("Fréquence %gGHz" % (frequence / 1000000000.))
    elif (frequence > 800000):
        print("Fréquence %gMHz" % (frequence / 1000000.))
    elif (frequence > 800):
        print("Fréquence %gKHz" % (frequence / 1000.))
    else:
        print("Fréquence %gHz" % (frequence))
    print("<br/>")
    print ("</td>")
    print("</tr>")
print("</table>")

print("""<input type=submit value="Mettre à jour les courbes"/><br/>""")

print("""Fréquence min: <input name="freq_min" value="%f"/><br/>""" % freq_min)
print("""Fréquence max: <input name="freq_max" value="%f"/><br/>""" % freq_max)

print("</form>")
print("""
<p>Notes</p>
<ul>
<li>Code source: <tt>git clone http://git.aquilenet.fr/tools.git</tt></li>
<li>V/m = sqrt(W/m^2 * 377)</li>
<li>W/m^2 = (V/m)^2 / 377
<li>E(V/m) = sqrt(30*PIRE(W))/d(m)</li>
<li>PIRE = E^2*d^2/30</li>
<li>µV/m = 10^(dBµV/m / 20)</li>
</ul>
</body>
</html>
""")
