(function() {

var $formRequest = $("#recherchend");

var $city = $formRequest.find("[name=city]");
var $zip = $formRequest.find("[name=zip]");
var $street = $formRequest.find("[name=street]");

function init() {
	var cityFetcher = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: "https://vador.fdn.fr/souscription/cherche-ville.cgi?etape=cherche_ville&query=%QUERY&zip=%ZIP",
			replace: function(url, query) {
				var the_zip = $zip.val();
				the_zip = the_zip !== undefined ? the_zip : "";
				url = url.replace("%QUERY", query);
				url = url.replace("%ZIP", the_zip);
				return url;
			},
			cache: false
		}
	});

	$city.typeahead({
		hint: true,
		highlight: true
	}, {
		name: "city",
		limit: Number.MAX_VALUE,
		source: cityFetcher
	});

	var streetFetcher = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: "https://vador.fdn.fr/souscription/cherche-rue.cgi?etape=cherche_rue&query=%QUERY&city=%CITY&zip=%ZIP",
			replace: function(url, query) {
				var the_zip = $zip.val();
				var the_city = $city.val();
				the_zip = the_zip !== undefined ? the_zip : "";
				the_city = the_city !== undefined ? the_city : "";
				url = url.replace("%QUERY", query);
				url = url.replace("%CITY", the_city);
				url = url.replace("%ZIP", the_zip);
				return url;
			},
			cache: false
		}
	});

	$street.typeahead({
		hint: true,
		highlight: true
	}, {
		name: "street",
		limit: Number.MAX_VALUE,
		source: streetFetcher
	});
}

$(document).ready(init);

})();
