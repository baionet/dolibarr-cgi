# -*- coding: utf-8 -*-
#
# Styles & co, à volonté
# 
# Ceci vient de drupal, réécrivez tout selon votre sauce

def header(title, typeahead=False):
	print("""
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    """)
	print("<title>%s</title>" % title)
	print("""
  <link rel=stylesheet type=text/css href="/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type=text/css href="http://sanssoucis.aquilenet.fr/static/css/google-titillium-200-400-600.css">
  <link rel="stylesheet" type=text/css href="/css_tools_aquilenet.css">
""")
	if typeahead:
		print("""
  <link href="/typeahead/typeaheadjs.css" rel=stylesheet />
  <script src="/typeahead/jquery.min.js"></script>
  <script src="/typeahead/typeahead.bundle.min.js"></script>
""")
	print("""
</head>

<body class="front not-logged-in page-node no-sidebars">
<!--
  <div class="navbar">
    <div class="navbar-inner">
      <div class="container">
        <a class="brand" href='http://tools.aquilenet.fr/'>Aquilenet Tools</a>
        <ul class="nav">
          <li><a href="http://www.aquilenet.fr/" title="">Site web</a></li> 
          <li><a href="https://adherents.aquilenet.fr/" title="">Espace adhérent(e)</a></li>
          <li><a href="http://listes.aquilenet.fr" title="Listes de discussions">Listes</a></li> 
          <li><a href="https://webmail.aquilenet.fr/" title="">Webmail</a></li> 
          <li><a href="http://atelier.aquilenet.fr" title="Site de travail de l&#039;association">Atelier</a></li> 
          <li><a href="http://pastebin.aquilenet.fr" title="Outil en ligne de publication de texte">Pastebin</a></li> 
          <li><a href="http://www.aquilenet.fr/contact" title="">Contact</a></li> 
        </ul>
      </div>
    </div>
  </div>
  -->

  <div class="container">
""")

def title2(title):
	print("""<h1 class="page-header">%s</h1>""" % title)

def footer():
	print("""
  <footer class="footer">
  	<p class="pull-right">Designed using the awesome <a
      href="http://twitter.github.com/bootstrap/">Twitter's bootstrap</a>.</p>
  </footer>

  </div><!-- container -->

  <script src="/bootstrap/js/jquery.js"></script>
  <script src="/bootstrap/js/bootstrap.js"></script>
</body>
</html>
""")
