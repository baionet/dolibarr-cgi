#!/usr/bin/python3
# coding: utf8

import os, sys
lang = os.getenv("LANG")
if lang != "fr_FR.UTF-8":
    os.putenv("LANG","fr_FR.UTF-8")
    os.execv(sys.argv[0], sys.argv)

import cgi
import cgitb
import os
import time
import sys
import math
from ondes_objets import *
cgitb.enable()

print("Content-Type: image/png\r")
print("\r")

sys.stdout.flush();

form = cgi.FieldStorage()
monform = dict((k,form[k].value) for k in form.keys())


unites = {
    "seconde": 1,
    "minute": 60,
    "heure": 60*60,
    "jour": 24*60*60,
    "semaine": 7*24*60*60,
    "mois": 30.5*24*60*60,
    "annee": 365.25*24*60*60,
}

freq_min=1
freq_max=1000000000000000

# Prendre en compte les valeurs passées en http
for objet in objets:
    nom,var,combien,unite,duree,distance,puissance,frequence = objet
    if var+"_combien" in monform:
        objet[2] = float(monform[var+"_combien"])
    if var+"_unite" in monform:
        objet[3] = monform[var+"_unite"] 
    if var+"_duree" in monform:
        objet[4] = monform[var+"_duree"]
    if var+"_distance" in monform:
        objet[5] = float(monform[var+"_distance"])
    if var+"_puissance" in monform:
        objet[6] = float(monform[var+"_puissance"])

if "freq_min" in monform:
    freq_min = float(monform["freq_min"])
if "freq_max" in monform:
    freq_max = float(monform["freq_max"])


p = os.popen("gnuplot5","w")
p.write("""
set terminal png size 1280,480 font 12
set xlabel "Fréquence"
set format x "%.s %cHz"
set ylabel "Exposition annuelle (an.V/m)"
set logscale x
set logscale y
set key bottom right
$mydata << EOD
""")
for objet in objets:
    nom,var,combien,unite,duree,distance,puissance,frequence = objet
    exposition = math.sqrt(30. * puissance) / distance
    exposition *= combien * unites[unite] * (unites["annee"] / unites[duree])
    p.write("%g %g \"%s\"\n"%(frequence,exposition/unites["annee"],nom))
p.write("""EOD
plot [x=%g:%g] \
        60 title "60V/m", \
        6 title "6V/m", \
        0.6 title "0.6V/m", \
        [x=1:25] 10000 title "limites légales" ls 0 lw 2, \
        [x=25:3000] 250/(x/1000) ls 0 lw 2 notitle, \
        [x=3000:1000000] 87 ls 0 lw 2 notitle, \
        [x=1000000:10000000] 87/sqrt(x/1000000) ls 0 lw 2 notitle, \
        [x=10000000:400000000] 28 ls 0 lw 2 notitle, \
        [x=400000000:2000000000] 1.375 * sqrt(x/1000000) ls 0 lw 2 notitle, \
        [x=2000000000:300000000000] 61 ls 0 lw 2 notitle, \
        $mydata with labels notitle
""" % (freq_min, freq_max))
p.close()
