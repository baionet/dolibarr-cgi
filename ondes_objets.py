# coding: utf8
objets = [

        # Nom visible, variable, combien, unité, durée, distance(m), PIRE(W), Fréq(Hz)

        # :http://www.radiofrequences.gouv.fr/spip.php?article39

        # http://www.who.int/peh-emf/about/WhatisEMF/fr/index3.html
        # 50V/m à 30cm
        ["Aspirateur", "aspirateur", 1, "heure", "semaine", 1, 7, 50],
        # 80V/m à 30cm
        ["Grille-pain", "grille", 20, "minute", "jour", 2, 19, 50],
        # 100V/m à 30cm
        ["Mixeur", "mixeur", 10, "minute", "jour", 1, 30, 50],
        # 80V/m à 30cm
        ["Seche-cheveux", "seche", 3, "minute", "jour", 0.2, 19, 50],
        # 120V/m à 30cm
        ["Fer à repasser", "fer", 1, "heure", "semaine", 0.3, 43, 50],
        # 180V/m à 30cm
        ["Poste Radio FM", "radioc", 1, "heure", "jour", 2, 97, 50],
        # 60V/m à 30cm
        ["TV", "telec", 3, "heure", "jour", 2, 10, 50],
        # 120V/m à 30cm
        ["Frigo", "frigo", 24, "heure", "jour", 5, 43, 50],

        # http://www.protection-onde.fr/les-ondes-au-quotidien
        ["Lampe basse consommation", "lampe", 6, "heure", "jour", 0.3, 0.6, 50],

        # http://www.proximit-e.cg54.fr/fr/DefautBureau.aspx?Onglet_ID=1306
        ["Plaque à induction", "induction", 1, "heure", "jour", 1, 0.2, 25000],

        ["Carte sans contact, badge", "NFC", 1, "minute", "jour", 0.010, 0.001, 13560000],

        ["Cibi", "cibi", 24, "heure", "jour", 1000, 4, 27000000],
        ["Babyphone", "baby", 2, "heure", "jour", 3, 0.01, 40000000],
        ["Radio FM", "radior", 24, "heure", "jour", 1000, 500, 100000000],
        ["TV", "teler", 24, "heure", "jour", 1000, 500, 500000000],

        # http://www.protection-onde.fr/les-ondes-au-quotidien
        ["Bluetooth3 court", "bluetoothc", 2, "heure", "jour", 0.5, 0.001, 2400000000],
        ["Bluetooth2 moyen", "bluetoothm", 2, "heure", "jour", 10, 0.0025, 2400000000],
        ["Bluetooth1 long", "bluetoothl", 2, "heure", "jour", 50, 0.100, 2400000000],

        ["Four micro-onde", "four", 10, "minute", "jour", 1, 0.2, 2450000000],

        ["Tel Sans fil", "telephoneDCT", 24, "heure", "jour", 5, 0.01, 1900000000],
        ["Appel Tel Sans fil", "telephoneDCTa", 1, "heure", "jour", 0.05, 0.25, 1900000000],

        ["Talkie Walkie", "talkie", 6, "heure", "jour", 0.5, 0.5, 446000000],

        ["Ordinateur", "ordi", 4, "heure", "jour", 0.5, 0.1, 2300000000],

        ["Wifi 2.5GHz", "wifi", 24, "heure", "jour", 5, 0.1, 2450000000],

        # La loi autorise 1W, en pratique on utilise 0.05W en moyenne
        ["Wifi 5Ghz R.", "wifi5r", 24, "heure", "jour", 100, 0.1, 5500000000],
        ["Wifi 5Ghz C.", "wifi5c", 24, "heure", "jour", 5, 0.1, 5500000000],

        ["Wimax C.", "wimaxc", 24, "heure", "jour", 1, 5, 3500000000],
        ["Wimax R.", "wimaxr", 24, "heure", "jour", 100, 20, 3500000000],

        # 20-50W
        ["R.GSM", "relai900", 24, "heure", "jour", 100, 35, 900000000],
        ["R.2G", "relai1800", 24, "heure", "jour", 100, 35, 1800000000],
        ["R.3G", "relai2100", 24, "heure", "jour", 100, 35, 2100000000],
        ["R.4G", "relai2600", 24, "heure", "jour", 100, 35, 2600000000],

        ["C.GSM", "telephone900", 24, "heure", "jour", 0.5, 2, 900000000],
        ["C.2G", "telephone1800", 24, "heure", "jour", 0.5, 2, 1800000000],
        ["C.3G", "telephone2100", 24, "heure", "jour", 0.5, 2, 2100000000],
        ["C.4G", "telephone2600", 24, "heure", "jour", 0.5, 2, 2600000000],

        ["A.GSM",  "telephoneappel900", 1, "heure", "jour", 0.05, 2, 900000000],
        ["A.2G", "telephoneappel1800", 1, "heure", "jour", 0.05, 2, 1800000000],
        ["A.3G", "telephoneappel2100", 1, "heure", "jour", 0.05, 2, 2100000000],
        ["A.4G", "telephoneappel2600", 1, "heure", "jour", 0.05, 2, 2600000000],

        # En gros 1kW/m2, donc environ 600V/m
        ["Soleil", "soleil", 12, "heure", "jour", 150000000000, 270000000000000000000000000, 500000000000000],

        # TODO: CPL
]
