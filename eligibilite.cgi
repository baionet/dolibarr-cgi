#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011, 2013-2021 Samuel Thibault <samuel.thibault@ens-lyon.org>
# Copyright (c) 2012 Guillaume Subiron <maethor@subiron.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 

import os, sys
lang = os.getenv("LANG")
if lang != "fr_FR.UTF-8":
    os.putenv("LANG","fr_FR.UTF-8")
    os.execv(sys.argv[0], sys.argv)

import cgi
import cgitb
cgitb.enable()
if sys.version_info[0] < 3:
    import httplib
else:
    import http.client as httplib
import json
import locale

import check
import html

import mymail
import copy

tarifs = {
	"particulier": {
		"degroupe_redux" : (90,25),
		"degroupe" : (90,29),
		"nondegroupe" : (80,39),
		"nondegroupe8" : (80,39),
		"LiazoOrange" : (72, 29),
		"LiazoOrangeV" : (72, 29),
		"LiazoOrangeNu" : (78, 40),
		"LiazoOrangeNuV" : (78, 40),
	},
	"preferentiel": {
		"degroupe_redux": (70,23.50),
		"degroupe": (70,27),
		"nondegroupe": (64,36),
		"nondegroupe8": (64,36),
		"LiazoOrange" : (72, 27),
		"LiazoOrangeV" : (72, 27),
		"LiazoOrangeNu" : (78, 38),
		"LiazoOrangeNuV" : (78, 38),
	}
}

upload = {
	"128k"  : "64k",
	"512k"  : "128k",
	"512RE" : "128k",
	"1M"    : "256k",
	"2M"    : "256k",
	"3M"    : "256k à 512k",
	"4M"    : "512k",
	"5M"    : "512k",
	"6M"    : "512k",
	"8M"    : "860k",
	"18M"   : "≤ 860k",
	"80M"   : "25M",
}

download = {
	"128k"  : 128,
	"512k"  : 512,
	"512RE" : 512,
	"1M"    : 1000,
	"2M"    : 2000,
	"3M"    : 3000,
	"4M"    : 4000,
	"5M"    : 5000,
	"6M"    : 6000,
	"8M"    : 8000,
	"18M"   : 18000,
	"80M"   : 80000,
}

print("Content-type: text/html; charset=utf-8\r\n\r\n")
locale.setlocale(locale.LC_ALL,"fr_FR.UTF-8")

#cgitb.enable(display=0, logdir="/tmp")

# On peut arriver ici avec des champs déjà remplis. C'est notamment le cas
# quand on clique sur un lien de souscription depuis l'espace adhérent (plutôt
# qu'intégrer la souscription à l'espace adhérent).

form = cgi.FieldStorage()
monform = dict((k,form[k].value) for k in form.keys())
if "adsltel" in monform:
	monform["adsltel"] = monform["adsltel"].replace(" ","").replace("\n","").replace("\t","")
champs_hidden=["adherent","personne","prenom","nom","adr","adrbis","cp","ville","mail","tel","typend","tarif","prelevement","iban","bic"]
vals={}
for id in champs_hidden:
	if id in monform:
		vals[id] = monform[id]
	else:
		vals[id] = ""

# Format de l'affichage d'une erreur
def erreur(msg):
	html.title2("Erreur")
	print("<div class='alert'><strong>Erreur : </strong>" + msg + "</div>")
	url = "test.cgi?"
	if "adsltel" in vals:
		url += "&adsltel=" + vals["adsltel"]
	if "cp" in vals:
		url += "&cp=" + vals["cp"]
	print("<a href='"+url+"' class='btn btn-large btn-primary'>Réessayer</a>")

# Véritable coeur du code. En tant que fonction pour pouvoir utiliser return
def doit():
	# On veut tous les paramètres
	if "adsltel" not in monform:
		erreur("Il faut renseigner le numéro de téléphone (ainsi que le code postal et le tarif).")
		return
	if "cp" not in monform:
		erreur("Il faut renseigner le code postal (ainsi que le numéro de téléphone et le tarif).")
		return
	if "tarif" not in monform:
		erreur("Il faut renseigner le tarif (ainsi que le numéro de téléphone et le code postal).")
		return

	# Vérifications des paramètres
	tel = monform["adsltel"]
	s = check.tel(tel)
	if s:
		erreur(s)
		return

	cp = monform["cp"]
	s = check.cp(cp)
	if s:
		erreur(s)
		return

	tarif = monform["tarif"]
	if tarif not in tarifs:
		erreur("Il faut choisir un des tarifs proposés")
		return
	letarif = tarifs[tarif]
	if tel[0:6] == "VOISIN":
		telpur = tel[6:]
	else:
		telpur = tel

	if "typend" in monform:
		typend = monform["typend"]
	else:
		# Interfaçage avec FDN pour savoir s'il y a plusieurs types de
		# NDI pour ce numéro de NDI
		c = httplib.HTTPSConnection("vador.fdn.fr")
		c.request("GET", "/souscription/nd-info.cgi?etape=nd_info&tel="+telpur)
		r = c.getresponse()
		d = r.read()
		c.close()
		v = json.loads(d.decode("utf-8"))
		adr_actif = v["actif"]
		adr_inactif = v["inactif"]
		adr_precab = v["precab"]
		n = 0
		if adr_actif:
			n+=1
			typend = "actif"
		if adr_inactif:
			n+=1
			typend = "inactif"
		if adr_precab:
			n+=1
			typend = "precab"
		if n == 0:
			erreur("Ce numéro n'apparaît pas dans les bases de lignes téléphoniques. C'est peut-être un numéro de téléphone via ADSL ? Cela ne nous permettra pas de passer commande, il nous faut le numéro de la ligne téléphonique sous-jacente. Si vous ne le connaissez pas, vous pouvez <a href='https://tools.aquilenet.fr/cgi-bin/recherchend.cgi'>passer par votre adresse.</a>")
			return

		if n > 1:
			# Ambiguité, il faut faire choisir par l'abonné
			html.title2("Précision du type du numéro "+tel)
			print("<div class='well'>")
			print('<p class="help-block">Le numéro de téléphone que vous avez fourni correspond à plusieurs logements, probablement suite à un déménagement avec portabilité de numéro. Il faut donc choisir le bon cas ci-dessous, sinon la livraison sera faite au mauvais endroit, et il faudra recommencer avec à la fois des frais d\'écrasement et de reconstruction.</p>')
			print('<form name="offre" method="post" action="eligibilite.cgi">')
			l = copy.copy(champs_hidden)
			l.remove("typend")
			for id in l:
				print("""<input type="hidden" id="%s" name="%s" value="%s"/>"""%(id,id,vals[id]))
			print("""<input type="hidden" id="%s" name="%s" value="%s"/>"""%("adsltel","adsltel",monform["adsltel"]))
			choix = u""
			print("<table class='table table-bordered table-hover'>")
			def format_addr(addr):
				return u"et votre adresse est: <br/>"+addr["name"]+"<br/>"+addr["street_nb"]+" "+addr["street"]+"<br/>"+addr["city"]+"<br/>"
			if adr_actif:
				choix += u'<tr><td><input type="radio" id="typendactif" name="typend" value="actif">Numéro actif: vous avez un abonnement Orange à ce numéro '+format_addr(adr_actif)+'</input></td></tr>\n'
			if adr_inactif:
				choix += u'<tr><td><input type="radio" id="typendinactif" name="typend" value="inactif">Numéro inactif: vous n\'avez pas d\'abonnement Orange à ce numéro '+format_addr(adr_inactif)+'</input></td></tr>\n'
			if adr_precab:
				choix += u'<tr><td><input type="radio" id="typendinactif" name="typend" value="inactif">Numéro pré-câblé: vous n\'avez pas d\'abonnement Orange à ce numéro, c\'est un logement neuf qui a été pré-câblé par Orange '+format_addr(adr_precab)+'</input></td></tr>\n'
			print(choix)
			print("</table>")
			print('<input type="submit" class="btn btn-primary" name="submit" id="submit" value="Valider">')
			print("</form>")
			print("</div>")
			return

	# Ok, on peut y aller
	html.title2("Test de la ligne "+tel+" au code postal "+cp)

	# Interfaçage avec FDN
	c = httplib.HTTPSConnection("vador.fdn.fr")
	c.request("GET", "/souscription/eligibilite.cgi?etape=eligibilite&tel="+telpur+"&cp="+cp+"&typend="+typend+"&tarif=blanche&liazo=1")
	r = c.getresponse()
	d = r.read()
	c.close()
	v = json.loads(d.decode("utf-8"))
	#print("réponse brute: <br/>")
	#print(v)
	#print("<br/>")

	une_erreur = False
	# Erreurs éventuelles
	if v["erreur"] != 0:
		print("<div class='alert alert-error'>Oops, il y a eu un problème: %d.</div>" % v["erreur"])
		une_erreur = True

	if "message" in v and v["message"]:
		print("<tt><pre>"+v["message"]+"</pre></tt>")
		print("<div class='alert'>Peut-être votre ligne est dégroupée totalement ? Il est alors normal que le test échoue puisque la ligne n'est plus dans les tables de FT. Essayez avec le numéro de téléphone d'un(e) voisin(e) qui n'a pas de dégroupage total, ou bien sur <a href=http://www.degrouptest.com>degrouptest.com</a>. Si votre ligne n'est pas dégroupée totalement, peut-être que le test de dégroupage est simplement en maintenance. Voici les motifs de rejet:</div>")
		print("<tt><pre>"+v["rejets"]+"</pre></tt>")
		une_erreur = True

	if "info_ligne" not in v or "offres" not in v:
		print("<div class='alert alert-error'>Oops, le test de dégroupage a l'air en maintenance.</div>")
		une_erreur = True

	if une_erreur:
		print("<a href='http://tools.aquilenet.fr/cgi-bin/test.cgi' class='btn btn-large btn-primary'>Réessayer</a>")
		return

	# Ok, pas de soucis ! Montrer les infos et choix
	print("<h3>Informations sur la ligne</h3>")
	print('<p class="help-block"><ul>')
	if ("nra" in v["info_ligne"] and v["info_ligne"]["nra"] != None):
	    print('<li class="help-block">NRA : ' + v["info_ligne"]["nra"] + "</li>");
	if ("length" in v["info_ligne"] and v["info_ligne"]["length"] != None):
	    print('<li class="help-block">Distance du DSLAM : ' + v["info_ligne"]["length"] + "m</li>");
	if ("attenuation" in v["info_ligne"] and v["info_ligne"]["attenuation"] != None):
	    print('<li class="help-block">Atténuation théorique : ' + v["info_ligne"]["attenuation"] + "dB</li>");
	if ("debit" in v["info_ligne"] and v["info_ligne"]["debit"] != None):
	    print('<li class="help-block">Débit IP théorique : ' + str(v["info_ligne"]["debit"]) + "Mbps</li>");
	print('<li class="help-block">Type de numéro : ' + typend + "</li>");
	print("</ul></p>")
	print("<div class='well'>")
	print('<form name="offre" method="post" action="adhesion.cgi">')
	print("<table class='table table-bordered table-hover'>")
	print("<th></th><th>option</th><th>débit<br>descendant<br>(bps)</th><th>débit<br>montant<br>(bps)</th><th>FAS<br>(TTC)</th><th>mensuel<br>(TTC)</th><th></th>")

	tarif_reduit = "Tarif réduit sur "
	for offre in v["offres"]:
		code = offre["code_offre"]
		if (("512k" in code) or ("512RE" in code)) and ("opt0" in code or "opt1" in code):
			offre_redux = offre.copy()
			offre_redux["redux"] = True
			v["offres"] = [ offre_redux ] + v["offres"]

	# normaliser les résultats
	for offre in v["offres"]:
		code = offre["code_offre"]
		offre["code"] = code
		pos = code.find("Liazo")
		if pos == -1:
		    pos = code.find("opt")
		debit = code[:pos]
		opt = code[pos:]
		if debit in download:
			descendant = download[debit]
		else:
			descendant = 0
			mymail.mymail(To="Samuel Thibault <samuel.thibault@aquilenet.fr>", From="webmaster@aquilenet.fr", Subject="Probleme eligibilite ligne ADSL", Body=("Oops, debit descendant inconnu pour "+code))
		offre["descendant"] = descendant

		if "opt0" in code:
			code_debit = code[0:-4]
			degroupage = " dégroupé partiellement par Nerim"
			if "redux" in offre:
				fas,mensuel = letarif["degroupe_redux"]
				degroupage += ", avec réduction offerte par Aquilenet"
			else:
				fas,mensuel = letarif["degroupe"]
		elif "opt1" in code:
			code_debit = code[0:-4]
			degroupage = " dégroupé partiellement par SFR via Nerim"
			if "redux" in offre:
				fas,mensuel = letarif["degroupe_redux"]
				degroupage += ", avec réduction offerte par Aquilenet"
			else:
				fas,mensuel = letarif["degroupe"]
		elif "opt3" in code:
			code_debit = code[0:-4]
			degroupage = " non dégroupé (ACA) par Orange via Nerim"
			if code in ("8M", "10M", "14M", "18M"):
				fas,mensuel = letarif["nondegroupe8"]
			else:
				fas,mensuel = letarif["nondegroupe"]
		elif "opt5" in code:
			code_debit = code[0:-4]
			degroupage = " non dégroupé (IP/ADSL) par Orange via Nerim"
			if code in ("8M", "10M", "14M", "18M"):
				fas,mensuel = letarif["nondegroupe8"]
			else:
				fas,mensuel = letarif["nondegroupe"]
		elif "LiazoOrangeNu" in code:
			code_debit = code[0:-13]
			if code in ("80MLiazoOrangeNu"):
				degroupage = " dégroupé totalement VDSL Orange via Liazo<br/><b>Attention, il <i>faut</i> utiliser un modem compatible VDSL, un modem ADSL ne fonctionnera pas</b>"
				fas,mensuel = letarif["LiazoOrangeNuV"]
			else:
				degroupage = " dégroupé totalement Orange via Liazo"
				fas,mensuel = letarif["LiazoOrangeNu"]
		elif "LiazoOrange" in code:
			code_debit = code[0:-11]
			if code in ("80MLiazoOrange"):
				degroupage = " non dégroupé VDSL Orange via Liazo<br/><b>Attention, il <i>faut</i> utiliser un modem compatible VDSL, un modem ADSL ne fonctionnera pas</b>"
				fas,mensuel = letarif["LiazoOrangeV"]
			else:
				degroupage = " non dégroupé Orange via Liazo"
				fas,mensuel = letarif["LiazoOrange"]
		else:
			degroupage = " dégroupage inconnu ?!"
			fas,mensuel = None,None

		offre["fas"] = fas
		offre["mensuel"] = mensuel
		offre["degroupage"] = degroupage
		offre["code_debit"] = code_debit

	lastopt=""
	for offre in v["offres"]:
		code = offre["code"]
		pos = code.find("Liazo")
		if pos == -1:
		    pos = code.find("opt")
		debit = code[:pos]
		opt = code[pos:]
		descendant = offre["descendant"]
		if debit in upload:
			montant = upload[debit]
		else:
			montant = "??"
			mymail.mymail(To="Samuel Thibault <samuel.thibault@aquilenet.fr>", From="webmaster@aquilenet.fr", Subject="Probleme eligibilite ligne ADSL", Body=("Oops, debit ascendant inconnu pour "+code))

		fas = offre["fas"]
		mensuel = offre["mensuel"]
		degroupage = offre["degroupage"]
		code_debit = offre["code_debit"]

		if tel[0:6] == "VOISIN" and not "LiazoOrangeNu" in opt:
			# Obligé d'avoir un dégroupage total pour une commande par voisin
			continue

		if descendant != 0:
			def find_better():
				for offre2 in v["offres"]:
					descendant2 = offre2["descendant"]
					if descendant2 != 0 and descendant2 > descendant and fas == offre2["fas"] and mensuel == offre2["mensuel"] and degroupage == offre2["degroupage"]:
						return True
				return False
			if find_better():
				continue

		if (opt != lastopt):
			print("<tr><td colspan=6></td></tr>")
			lastopt = opt
		print("<tr>")
		print('<td align="center"><input type="radio" id="'+offre["code_offre"]+'" name="option" value="'+offre["code_offre"]+'" required/></td>')
		print('<td align="center">jusque '+code_debit+degroupage+'</td><td align="center">'+offre["debit"]+"</td>")
		print('<td>'+montant+'</td>')
		if fas != None and mensuel != None:
			print('<td align="center">'+str(fas)+'€</td> <td align="center">'+str(mensuel)+"€</td>")
		if "reserve" in offre and offre["reserve"] != None:
		    print('<td>'+offre["reserve"]+'</td>')
		print("</tr>")
	print("</table>")
	for id in champs_hidden:
		print("""<input type="hidden" id="%s" name="%s" value="%s"/>"""%(id,id,vals[id]))
	print('<input type="hidden" name="adsltel" value="'+tel+'">')
	print('<input type="hidden" name="firstsubmit" value="yes">')
	print("<div class='alert alert-info'>")
	print("<p><strong>Note :</strong> les FAS (Frais d'Accès au Service), habituellement cachés par les fournisseurs d'accès commerciaux, sont ici explicités car facturés à l'ouverture de la ligne (mais vous pouvez demander à étaler leur paiement).</p>")
	print("<p><strong>Note2 :</strong> Si le débit choisi est 80 Mb/s, il faut disposer d'un modem compatible VDSL2 (aussi appelé G.993.2), sinon cela ne fonctionnera pas du tout.</p>");
	print("<p><strong>Note3 :</strong> Si plus tard vous voudrez changer de débit, il faudra repayer une construction de ligne.</p>");
	print("<p><strong>Note4 :</strong> Les choix en dégroupage total permettent de se passer de l'abonnement téléphone chez France Télécom, c'est pour cela qu'elles sont plus chères: c'est le coût de l'entretien de la ligne de cuivre, que l'abonnement téléphone incluait.</p>")
	print("<p><strong>Note5 :</strong> Pour les autres choix (non dégroupé ou dégroupé partiellement) vous devez garder votre ligne téléphonique chez France Télécom, et les frais pour la ligne téléphonique (typiquement 18€) s'ajoutent donc à notre prix, qui n'inclut <b>que</b> la partie ADSL.</p>")
	#print("<p><strong>Note6 :</strong> ACA et IP/ADSL sont essentiellement identiques d'un point de vue abonné, les débits disponibles varient simplement selon les lignes. Prenez simplement le plus grand, et en cas d'égalité, peu importe.</p>")
	print("</div>")
	print("<div class='form-actions'>")
	print('<input type="submit" class="btn btn-primary" name="submit" id="submit" value="Commander la ligne">')
	print("</div>")
	print("</form>")
	print("</div>")
	print("<br />")
	print('<p class="help-block">Les autres options ont été éliminées pour les raisons suivantes:</p>')
	print("<tt><pre>"+v["rejets"]+"</pre></tt>")



# code principal, rien d'intéressant
html.header("Résultats d'éligibilité ADSL")
doit()
html.footer()
