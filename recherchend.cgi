#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011, 2013-2020 Samuel Thibault <samuel.thibault@ens-lyon.org>
# Copyright (c) 2012 Guillaume Subiron <maethor@subiron.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
# EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 

import os, sys
lang = os.getenv("LANG")
if lang != "fr_FR.UTF-8":
    os.putenv("LANG","fr_FR.UTF-8")
    os.execv(sys.argv[0], sys.argv)

import cgi
import cgitb
cgitb.enable()
if sys.version_info[0] < 3:
    import httplib
else:
    import http.client as httplib
import json
import locale
import unidecode

import check
import html

import re

print("Content-type: text/html; charset=utf-8\r\n\r\n")
locale.setlocale(locale.LC_ALL,"fr_FR.UTF-8")

#cgitb.enable(display=0, logdir="/tmp")

# On peut arriver ici avec des champs déjà remplis. C'est notamment le cas
# quand on clique sur un lien de souscription depuis l'espace adhérent (plutôt
# qu'intégrer la souscription à l'espace adhérent).

form = cgi.FieldStorage()
monform = dict((k,form[k].value) for k in form.keys())
champs=["street_nb","street","zip","city"]
champs_hidden=["do"]
vals={}
for id in champs + champs_hidden:
	if id in monform:
		vals[id] = unidecode.unidecode(monform[id])
	else:
		vals[id] = ""

# Format de l'affichage d'une erreur
def erreur(msg):
	html.title2("Erreur")
	print("<div class='alert'><strong>Erreur : </strong>" + msg + "</div>")
	print("<a href='recherchend.cgi?street_nb="+vals["street_nb"]+"&street="+vals["street"]+"&zip="+vals["zip"]+"&city="+vals["city"]+"' class='btn btn-large btn-primary'>Réessayer</a>")

# Véritable coeur du code. En tant que fonction pour pouvoir utiliser return
def doit():
	if vals["do"] != "1":
		vals["do"] = "1"
		html.title2("Recherche de numéro de ligne")
		print('  <p class="help-block">Pour le nom de la ville et de la rue, ne mettez que la partie principale et attendez quelques secondes pour que des choix apparaissent: par exemple, pour « rue du Lapin », indiquez juste « Lapin ».</p>')
		print('  <p class="help-block">Si vous ne trouvez pas votre ligne, essayez de ne pas mettre de numéro de rue, les immeubles sont renseignés à un seul de leurs numéros couverts par exemple.</p>')
		print("""
<form id="recherchend" class="well form-horizontal" method="post" action="recherchend.cgi">
<div class="control-group">
  <label class="control-label" for="zip">Code Postal</label>
  <div class="controls">
    <input type="text" id="zip" name="zip" value="%s"/>
  </div>
</div>
<div class="control-group">
  <label class="control-label" for="city">Ville</label>
  <div class="controls">
    <input type="text" id="city" name="city" value="%s"/>
  </div>
</div>
<div class="control-group">
  <label class="control-label" for="street">Rue</label>
  <div class="controls">
    <input type="text" id="street" name="street" value="%s"/>
  </div>
</div>
<div class="control-group">
  <label class="control-label" for="street_nb">Numéro</label>
  <div class="controls">
    <input type="text" id="street_nb" name="street_nb" value="%s"/>
  </div>
</div>
"""%(vals["street_nb"],vals["street"],vals["zip"],vals["city"]))
		for id in champs_hidden:
			print("""<input type="hidden" id="%s" name="%s" value="%s"/>"""%(id,id,vals[id]))
		print('  <div class="form-actions">')
		print('    <input type="submit" class="btn btn-primary" id="submit" name="submit" value="Valider"/>')
		print('  </div>')
		print("""<script src="/typeahead/default.js"></script>""")
		print("</form>")
		return
		#print("<table class='table table-bordered table-hover'>")
		#print("</table>")
		#print("<tr><td><input type=radio name=tel value=1234>numéro</input></td></tr>")

	# On veut tous les paramètres sauf peut-être le numéro
	if "street" not in monform or "zip" not in monform or "city" not in monform:
		erreur("Il faut renseigner au moins la rue, le code postal et la ville")
		return

	# Vérifications des paramètres
	cp = monform["zip"]
	s = check.cp(cp)
	if s:
		erreur(s)
		return

	s = ""
	if "street_nb" in monform and vals["street_nb"] != '':
		s = "&street_nb=" + vals["street_nb"]


	# Interfaçage avec FDN pour trouver les ND à une adresse
	c = httplib.HTTPSConnection("vador.fdn.fr")
	req = "/souscription/cherche-nd.cgi?etape=cherche_nd"+s+"&street="+vals["street"]+"&zip="+vals["zip"]+"&city="+vals["city"]
	req = req.replace(" ", "%20")
	c.request("GET", req)
	r = c.getresponse()
	d = r.read()
	c.close()
	v = json.loads(d.decode("utf-8"))
	l = v["liste"]

	if l == None:
		erreur("Rue ou ville non trouvée")
		return

	if len(l) == 0:
		erreur("Aucun numéro trouvé")
		if "nb_rouges" in v and v["nb_rouges"] > 0:
			if v["nb_rouges"] > 1:
				print('<p class="help-block">Note: '+str(v["nb_rouges"])+' numéros en liste rouge ont été filtrés, vous pouvez demander plus de détails à support@aquilenet.fr</p>')
			else:
				print('<p class="help-block">Note: un numéro en liste rouge a été filtré, vous pouvez demander plus de détails à support@aquilenet.fr</p>')
		return

	html.title2("Recherche de numéro de ligne ")
	print("<div class='well'>")
	print('<p class="help-block">Voici le choix des numéros correspondant à cette adresse, veuillez choisir le bon:</p>')
	print('<form name="choix" method="post" action="test.cgi">')
	print("<table class='table table-bordered table-hover'>")
	for i in l:
		#print(i)
		typend = "actif"
		if i["disabled"] == True:
			typend = "inactif"
		elif i["precab"] == True:
			typend = "precab"
		if i["nd"] != None:
			print('<tr><td><input type="radio" id="teltype" name="teltype" value="'+i["nd"]+"-"+typend+'">')
		else:
			print('<tr><td><input type="radio" id="teltype" name="teltype" value="'+i["ptfid"]+"-"+typend+'">')
		if i["nd"] != None:
			print(i["nd"])
		if i["ptfid"] != None:
			print(i["ptfid"])
		if typend == "precab":
			print(" pré-câblé")
		else:
			print(" "+typend)
		print("<br/>")
		print(re.sub(r'(?!\b).(?!\b)', ".", i["name"])+'<br/>')
		s = ""
		if i["ensemble"] != None:
			s += " ensemble "+i["ensemble"]+" "
		if i["building"] != None:
			s += " batiment "+i["building"]+" "
		if i["stairs"] != None:
			s += " escalier "+i["stairs"]+" "
		if i["floor"] != None:
			s += u" etage "+i["floor"]+" "
		if i["door"] != None:
			s += " porte "+i["door"]+" "
		if i["logo"] != None:
			s += " logo "+i["logo"]+" "
		if s:
			print(s+"<br/>")
		if i["street_nb"] != None:
			print(i["street_nb"].lstrip('0')+" ")
		if i["street"] != None:
			print(i["street"])
		print("<br/>")
		if "city" in i and i["city"] != None:
			print(i["city"]+"<br/>")
		print('</input></td></tr>')
	print("</table>")
	if "nb_rouges" in v and v["nb_rouges"] > 0:
		if v["nb_rouges"] > 1:
			print('<p class="help-block">Note: '+str(v["nb_rouges"])+' numéros en liste rouge ont été filtrés, vous pouvez demander plus de détails à support@aquilenet.fr</p>')
		else:
			print('<p class="help-block">Note: un numéro en liste rouge a été filtré, vous pouvez demander plus de détails à support@aquilenet.fr</p>')
	print('<input type="submit" class="btn btn-primary" name="submit" id="submit" value="Valider">')
	print("</form>")
	print("</div>")
	return

# code principal, rien d'intéressant
html.header("Résultats d'éligibilité ADSL", typeahead=True)
doit()
html.footer()
