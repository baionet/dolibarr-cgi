#!/usr/bin/python3
#-*- coding: utf-8 -*-

import os, sys
lang = os.getenv("LANG")
if lang != "fr_FR.UTF-8":
    os.putenv("LANG","fr_FR.UTF-8")
    os.execv(sys.argv[0], sys.argv)

import sys
import os
import codecs
import time

base = "/srv/si/incoming"

print("Content-type: text/html; charset=utf-8\r\n\r\n")

def file_date(a):
    st = os.stat(base + "/" + a)
    return st.st_ctime

l = sorted(os.listdir(base), key=file_date, reverse=True)

print("<ul>")
for filename in l:
    if filename == "old":
        continue
    date = time.localtime(file_date(filename))
    f = open(base + "/" + filename, encoding = "utf-8")
    _ = f.readline()
    prenom = f.readline()
    nom = f.readline()
    _ = f.readline()
    _ = f.readline()
    _ = f.readline()
    _ = f.readline()
    _ = f.readline()
    _ = f.readline()
    _ = f.readline()
    _ = f.readline()
    _ = f.readline()
    _ = f.readline()
    _ = f.readline()
    adsl = f.readline()
    _ = f.readline()
    _ = f.readline()
    _ = f.readline()
    vpn = f.readline()
    print('<li><a href="/incoming/incoming/' + filename + '" download>' + time.strftime("%F", date) + " " + prenom + " " + nom + " ")
    if adsl != "\n":
        print(" ADSL ")
    if vpn != '\n':
        print(" VPN ")
    print("</a></li>")
    f.close()
print("</ul>")
